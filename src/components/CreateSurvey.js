import React from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';



class CreateSurvey extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            title: "",
            category: "",
            openDate: null,
            closeDate: null,
            survey_categories: [],
            newSurvey: [],
            isCreated: false,
            errors: false,
            errorMsg: "",
            responseStatus: null,
        };
    }


    componentDidMount() {
        axios.get('https://peaceful-retreat-69027.herokuapp.com/survey-categories')
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    survey_categories: data,
                })
            })
            .catch((error) => console.error(error));
    }

    onSubmit = async (event) => {
        event.preventDefault();

        let { title, category, openDate, closeDate } = this.state;

        const data = {
            title,
            survey_category_uuid: category,
            openDate,
            closeDate
        };
        if (title !== "" && category !== "" && openDate !== "" && closeDate !== "") {
            try {
                console.log("Data ", data)

                const response = await axios(
                    {
                        method: 'post',
                        url: `https://peaceful-retreat-69027.herokuapp.com/surveys/create`,
                        data: data
                    });

                if (response.status === 200) {
                    const result = response.data;
                    console.log(result)
                    this.setState({
                        newSurvey: result,
                        errors: false,
                    });

                    this.props.history.push(`/create-survey/${this.state.newSurvey.uuid}/question-answers`);
                } else {
                    this.setState({
                        errors: true,
                        errorMsg: response.errorMsg,
                    });
                }

            } catch (error) {
                this.setState({
                    errors: true,
                    errorMsg: error,
                });
            }
        } else {
            this.setState({
                errors: true,
            });
        }
    }



    handleChange = (event) => {
        switch (event.target.name) {
            case 'title':
                this.setState({
                    title: event.target.value
                });
                break;
            case 'category':
                this.setState({
                    category: event.target.value
                });
                break;
            default: break;
        };
    }

    handleOpenDate = (date) => {
        this.setState({
            openDate: date
        });
    };

    handleCloseDate = (date) => {
        this.setState({
            closeDate: date
        });
    };



    render() {
        if (this.state.survey_categories.length > 0) {
            const categoriesComponent = this.state.survey_categories.map((category) =>
                <option key={category.uuid} value={category.uuid}>{category.category}</option>);
            return (
                <section className="create-survey-section flex-grow-1">
                    <div className='create-survey-card'>

                        <h1 className='heading'>Create Your Own Survey</h1>

                        <form onSubmit={this.onSubmit}>
                            <div className='create-survey-inputs form-group'>

                                <input name='title' type='text'
                                    className='form-control'
                                    placeholder='Enter your survey name'
                                    onChange={this.handleChange}
                                    required
                                />
                                <select className='form-control' name='category' onChange={this.handleChange} required>
                                    {categoriesComponent}
                                </select>

                            </div>
                            <div className="form-group create-survey-open-and-close-date">
                                <div id="open-datepicker">
                                    <p>Open Date</p>
                                    <DatePicker
                                        className='form-control'
                                        selected={this.state.openDate}
                                        onChange={(date) => this.handleOpenDate(date)}
                                        name="openDate"
                                        dateFormat="dd/MM/yyyy"
                                        minDate={new Date()}
                                        required
                                    />
                                </div>
                                <div id="close-datepicker">
                                    <p>Close Date</p>
                                    <DatePicker
                                        className='form-control'
                                        selected={this.state.closeDate}
                                        onChange={(date) => this.handleCloseDate(date)}
                                        name="closeDate"
                                        minDate={this.state.openDate}
                                        disabled={this.state.openDate === null}
                                        dateFormat="dd/MM/yyyy"
                                        required
                                    />
                                </div>
                            </div>
                            <div className='create-survey-btn form-group'>
                                <button onClick={this.props.closeModal} className='btn btn-lg btn-success'>Save Survey</button>
                            </div>
                        </form>

                        {(this.state.errors) === true ?
                            <div>
                                <p className='showError text-danger text-center'>Please check/select all the inputs fields.</p>
                                <p className='showError text-danger text-center'>Title should be minimum 8 characters long.</p>
                            </div>
                            : null}
                        {(this.state.errorMsg) !== "" ? <p className='showError text-danger text-center'>{this.state.errorMsg}</p> : null}

                    </div>
                </section>
            )
        } else {
            return null;
        }
    }
}
export default CreateSurvey;