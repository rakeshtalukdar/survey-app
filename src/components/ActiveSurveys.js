import React from 'react';
import Moment from 'react-moment';
import Sidebar from './Sidebar';
import axios from 'axios';


class ActiveSurveys extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSurveys: [],
            title: ""
        }
    }
    componentDidMount() {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/active/surveys`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    activeSurveys: data,
                })
            })
            .catch((error) => console.error('Error ', error));
    }
    render() {

        return (
            <main className='d-flex flex-wrap justify-content-between'>
                <Sidebar />
                <section id='active-surveys'>
                    <table className='table table-dark table-hover'>
                        <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Open date</th>
                                <th>Close date</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.activeSurveys.length > 0 && this.state.activeSurveys.map((survey, index) => {
                                return (
                                    <tr key={survey.uuid}>
                                        <td>{index + 1}</td>
                                        <td>{survey.title}</td>
                                        <td>{survey.category}</td>
                                        <td>{survey.status}</td>
                                        <td><Moment format={'DD/MM/YYYY'}>{survey.open_date}</Moment></td>
                                        <td><Moment format={'DD/MM/YYYY'}>{survey.close_date}</Moment></td>
                                        <td>{survey.closeDate}</td>

                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </section>
            </main>
        );
    }
}
export default ActiveSurveys;