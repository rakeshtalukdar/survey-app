import React from "react";
import '../index.css'

const Home = () => {

    return (
        <main id="home-main">
            <div>

                <h1>Welcome To Survey App</h1>
                <p>Let's Get Started</p>
            </div>
        </main>
    )
}

export default Home;