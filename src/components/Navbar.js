import React from 'react';
import { Link } from 'react-router-dom';


class Navbar extends React.Component {

    render() {

        return (

            <nav className='navbar navbar-expand-sm bg-dark'>
                <h1 className='navbar-brand text-warning'>Survey App</h1>
                <ul className='navbar-nav'>
                    <li className='nav-item'>
                        <Link to='/' className='nav-link'>Home</Link>
                    </li>
                    <li className='nav-item'>
                        <Link to='/surveys' className='nav-link'>Surveys</Link>
                    </li>

                    <li className='nav-item'>
                        <Link to='/create-survey' className='nav-link bg-success' id="create-survey-btn">Create Survey</Link>
                    </li>
                    <li className='nav-item'>
                        <Link to='/dashboard' className='nav-link'>Dashboard</Link>
                    </li>
                </ul>
            </nav>

        )
    }
};

export default Navbar;