import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';



class UserReviewSurvey extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            allQuestionsAndAnswers: [],
        }
        this.surveyUuid = props.match.params.id;
        this.userUuid = props.match.params.userId;
    }
    componentDidMount() {
        this.getAllQuestionsAndAnswers();
    }


    getAllQuestionsAndAnswers = () => {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/user-review-survey/${this.surveyUuid}/user/${this.userUuid}`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    allQuestionsAndAnswers: data,
                });
            })
            .catch((error) => console.error('Error ', error));
    }



    render() {
        if (this.state.allQuestionsAndAnswers.length > 0) {
            return (
                <section id="user-answer-review" className='flex-grow-1'>
                    <h1 className='heading'>Answer Review</h1>
                    {this.state.allQuestionsAndAnswers.map((response, index) => {
                        return (
                            <div key={response.uuid} className="card">
                                <h5 className="card-header">{index + 1}. {response.question}?</h5>
                                <div className="card-body">
                                    <ul className='list-group'>
                                        <li className='list-group-item'>{response.answer} </li>


                                    </ul>
                                </div>
                            </div>
                        )
                    })}

                    <div>
                        <Link to={`/user-survey/${this.userUuid}`}>
                            <button className='btn btn-md btn-success survey-submit-btn'>Back</button>
                        </Link>
                    </div>
                </section>
            )
        } else {
            return null;
        }
    }

}
export default UserReviewSurvey;