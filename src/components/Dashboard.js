import React from 'react';
import Sidebar from './Sidebar';
class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
        };
    }
    showModal = (event) => {
        event.preventDefault();
        this.setState({
            showModal: !this.state.showModal,
        });
    }

    render() {
        return (
            <main className='d-flex flex-wrap justify-content-between'>
                <Sidebar />
                <section className='dashboard-welcome flex-grow-1'>
                    <h1>Welcome To Survey App Dashboard</h1>
                </section>
            </main>
        )
    }
}
export default Dashboard;