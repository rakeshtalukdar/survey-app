import React from 'react';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import axios from 'axios';



class Surveys extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSurveys: [],
            dataFetched: false,
            errorMsg: ''
        }
    }

    componentDidMount() {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/active/surveys`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    activeSurveys: data,
                    dataFetched: true,
                    errorMsg: '',
                })
            })
            .catch((error) => {
                this.setState({
                    dataFetched: false,
                    errorMsg: 'Sorry!! Unable to find any survey.'
                });
            });
    }

    render() {
        if (this.state.dataFetched === true) {
            return (
                <section id='active-surveys'>
                    <table className='table table-dark table-hover'>
                        <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Open date</th>
                                <th>Close date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            {this.state.activeSurveys.length > 0 && this.state.activeSurveys.map((survey, index) => {
                                return (

                                    <tr key={survey.uuid}>
                                        <td>{index + 1}</td>
                                        <td><Link className='participate-survey-link links' to={`/participate-survey/${survey.uuid}`}>{survey.title}</Link></td>
                                        <td>{survey.category}</td>
                                        <td>{survey.status}</td>
                                        <td><Moment format={'DD/MM/YYYY'}>{survey.open_date}</Moment></td>
                                        <td><Moment format={'DD/MM/YYYY'}>{survey.close_date}</Moment></td>
                                        <td><Link className='participate-survey-link links' to={`/participate-survey/${survey.uuid}`}>
                                            <button className='btn btn-sm btn-success'>View</button> </Link>
                                        </td>
                                    </tr>
                                );

                            })}
                        </tbody>
                    </table>

                </section>

            )
        } else {
            return <p className='text-danger'>{this.state.errorMsg}</p>;
        }
    }
}
export default Surveys;