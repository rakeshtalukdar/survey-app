import React from 'react';

class CreateSurveyModal extends React.Component {
    // constructor(props) {
    //     super(props);
    // }
    handleChange = (event) => {
        console.log(event.target.value);
    }

    render() {
        if (this.props.showModal) {
            return (

                <div id='create-survey-modal' className='flex-grow-1'>
                    <form className='modal-form'>
                        <div className='modal-header form-group'>
                            <h1>Give a name to your survey</h1>
                            <button onClick={this.props.closeModal} className='btn btn-sm btn-danger'>X</button>
                        </div>
                        <div className='modal-inputs form-group'>

                            <input name='survey-name' className='form-control' placeholder='Enter your survey name' onChange={this.handleChange} />
                            <select className='form-control' name='survey-category'>
                                <option>Information Technology</option>
                                <option>Healthcare</option>
                                <option>Agriculture</option>
                                <option>Engineering</option>
                                <option>Cultural</option>
                            </select>
                        </div>
                        <div className='modal-footer form-group'>
                            <button onClick={this.props.closeModal} className='btn btn-md btn-danger'>Close</button>
                            <button onClick={this.props.closeModal} className='btn btn-md btn-success'>Save Survey</button>
                        </div>
                    </form>
                </div>

            );
        } else {
            return null;
        }
    }
}

export default CreateSurveyModal;