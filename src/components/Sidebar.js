import React from 'react';
import { Link } from 'react-router-dom';

const Sidebar = () => {
    return (
        <aside id='sidebar'>
            <Link to='/all-surveys' className="sidebar-link">All Surveys</Link>
            <Link to='/active-surveys' className="sidebar-link">Active Surveys</Link>
            <Link to='/closed-surveys' className="sidebar-link">Closed Surveys</Link>
            <Link to='/submitted-surveys' className="sidebar-link">Submitted Surveys</Link>
            <Link to='/users' className="sidebar-link">Users</Link>
        </aside>
    );
};

export default Sidebar;