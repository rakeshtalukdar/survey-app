import React from 'react';
import axios from 'axios';

class QuestionAnswers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            question: "",
            selectedType: "",
            openAns: '',
            mcqAns1: '',
            mcqAns2: '',
            mcqAns3: '',
            mcqAns4: '',
            booleanAns1: '',
            booleanAns2: '',
            mcqOptionalAns1: '',
            mcqOptionalAns2: '',
            mcqOptionalAns3: '',
            mcqOptionalAns4: '',
            questionTypes: [],
            questionAnswers: [],
            allQuestions: [],
            allAnswers: [],
            dataLoaded: false,
            dataPosted: false,
            errors: false,
            errorMsg: '',
            msg: '',
            surveyUuid: '',
        }
        this.surveyUuid = props.match.params.id;;

    }

    componentDidMount() {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/question-types`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    questionTypes: data,
                    dataLoaded: true,
                })
            })
            .catch((error) => console.error('Error ', error));
        this.getAllAnswers();
        this.getAllQuestions();

    }

    handleTypeChange = (event) => {
        event.preventDefault();
        this.setState({
            selectedType: event.target.value,
        });
    }


    handleChange = (event) => {
        event.preventDefault();
        this.setState({
            msg: '',
        })
        const name = event.target.name;
        const value = event.target.value;
        if (event.target.validity.valid) {
            this.setState({
                [name]: value,
            });
        }
        if (name === 'question' && value.length < 6) {
            this.setState({
                errorMsg: 'Question should have minimum 6 characters'
            });
        } else if (name === 'question' && value.length >= 6) {
            this.setState({
                errorMsg: null
            });
        }
    }


    handleSubmit = async (event) => {
        event.preventDefault();

        const {
            question,
            selectedType,
            mcqAns1,
            mcqAns2,
            mcqAns3,
            mcqAns4,
            booleanAns1,
            booleanAns2,
            mcqOptionalAns1,
            mcqOptionalAns2,
            mcqOptionalAns3,
            mcqOptionalAns4,
        } = this.state;

        const data = {
            survey_uuid: this.surveyUuid,
            question,
            question_type: selectedType,
        }

        switch (selectedType) {
            case 'Open':
                data["openAns"] = '';
                break;
            case 'Number':
                data["numberAns"] = '';
                break;
            case 'Currency':
                data["currencyAns"] = '';
                break;
            case 'MCQ':
                data["mcqAns1"] = mcqAns1;
                data["mcqAns2"] = mcqAns2;
                data["mcqAns3"] = mcqAns3;
                data["mcqAns4"] = mcqAns4;
                break;
            case 'MCQ_Optional':
                data["mcqOptionalAns1"] = mcqOptionalAns1;
                data["mcqOptionalAns2"] = mcqOptionalAns2;
                data["mcqOptionalAns3"] = mcqOptionalAns3;
                data["mcqOptionalAns4"] = mcqOptionalAns4;
                break;
            case 'Boolean':
                data["booleanAns1"] = booleanAns1;
                data["booleanAns2"] = booleanAns2;
                break;
            default: break;
        }


        if (question !== '' && selectedType !== '') {
            try {
                const response = await axios(
                    {
                        method: 'post',
                        url: `https://peaceful-retreat-69027.herokuapp.com/surveys/${this.surveyUuid}/questions/create`,
                        data,
                    })
                if (response.status === 200) {
                    this.setState({
                        dataPosted: true,
                        errors: false,
                        errorMsg: '',
                        msg: 'Data Successfully inserted.'
                    });
                }
                else if (response.status === 422) {
                    this.setState({
                        errors: true,
                        errorMsg: `${response.statusText}. Please check your inputs`,
                        msg: '',
                    });
                }
            } catch (error) {
                this.setState({
                    errors: true,
                    errorMsg: error,
                    msg: '',
                });
            }
        } else {
            this.setState({
                errors: true,
                errorMsg: `Please provide / select all input fields`,
                msg: '',
            });
        }
    }


    getAllQuestions = () => {

        axios.get(`https://peaceful-retreat-69027.herokuapp.com/${this.surveyUuid}/all-questions`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    allQuestions: data,
                });
            })
            .catch((error) => console.error('Error ', error));
    }

    getAllAnswers = () => {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/all-answers`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    allAnswers: data,
                });
            })
            .catch((error) => console.error('Error ', error));
    }


    handleDelete = async (event, uuid) => {
        event.preventDefault();
        try {
            const response = await axios.delete(`https://peaceful-retreat-69027.herokuapp.com/surveys/${this.surveyUuid}/questions/${uuid}`)
            if (response.status === 200) {
                const result = await response.data;
                alert(result.msg)
                this.setState({
                    msg: result.msg,
                    errors: false,
                    isDeleted: true,
                });

            } else {
                this.setState({
                    errors: true,
                });
            }

        } catch (error) {
            this.setState({
                errors: true,
            });
        }

    };

    render() {
        if (this.state.dataLoaded === true) {
            return (
                <main id="questions-answers">
                    <section id="question-answer-field">
                        <h1 className='heading'>Added Questions And Answers</h1>
                        <form className='admin-question-answer' onSubmit={this.handleSubmitResponse}>

                            {this.state.questionTypes.length > 0 && this.state.allQuestions.map((question) => {
                                return (
                                    <div key={question.uuid + 12} className="admin-survey-card card">
                                        <h5 className="card-header">{question.question}? </h5>
                                        <div key={question.uuid + 3} className="card-body">
                                            {this.state.allAnswers.length > 0 && this.state.allAnswers.map((answer, index) => {

                                                if (question.uuid === answer.question_uuid) {
                                                    if (question.qType === 'Open') {
                                                        return (
                                                            < textarea key={answer.uuid}
                                                                id={answer.uuid}
                                                                onChange={this.handleAnswer}
                                                                name={question.uuid}
                                                                className="card-text"
                                                            />
                                                        );
                                                    }

                                                    if (question.qType === 'Number') {
                                                        return (
                                                            <li key={answer.uuid} className='list-group-item'>

                                                                <input type="text"
                                                                    id={answer.uuid} required
                                                                    onChange={this.handleAnswer}
                                                                    name={question.uuid}
                                                                    className="card-text"
                                                                    placeholder='Enter a valid number'
                                                                    pattern="[0-9]*"
                                                                />
                                                            </li>
                                                        );
                                                    }

                                                    if (question.qType === 'Currency') {
                                                        return (
                                                            <li key={answer.uuid} className='list-group-item'>

                                                                <input type="text"
                                                                    id={answer.uuid} required
                                                                    onChange={this.handleAnswer}
                                                                    name={question.uuid}
                                                                    className="card-text"
                                                                    placeholder='Enter a valid currency'
                                                                    pattern="[0-9].*"
                                                                />
                                                            </li>
                                                        );
                                                    }

                                                    else {
                                                        return (
                                                            <label key={answer.uuid + 12}>
                                                                <input key={index} id={answer.uuid}
                                                                    onChange={this.handleAnswer}
                                                                    type='radio' name={question.uuid}
                                                                    className="card-text"
                                                                    value={answer.answer}
                                                                />
                                                                {answer.answer}
                                                            </label>
                                                        )
                                                    }
                                                } else {
                                                    return null;
                                                }
                                            })}
                                        </div>
                                        <div className='card-footer'>
                                            <button onClick={(event) => this.handleDelete(event, question.uuid)} className="btn btn-danger btn-sm admin-survey-delete-btn">Delete</button>

                                        </div>
                                    </div>
                                )

                            })}
                        </form>

                    </section>


                    <aside id="add-question-answer-sidebar">
                        <div className="header">
                            <h1 className='question-panel-header' >Add Question</h1>
                        </div>
                        <div className='error-display'>
                            <p className='text-success pl-5 pt-5'>{this.state.msg}</p>
                            <p className='text-danger pl-5 pt-5'>{this.state.errorMsg}</p>
                        </div>
                        <div className='question-panel-input-field'>
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group col-md-12">
                                    <label>Question</label>
                                    <input type="text"
                                        className="form-control"
                                        id="inputQuestion"
                                        placeholder='Enter your question'
                                        name='question'
                                        required
                                        onChange={this.handleChange} />
                                </div>
                                <div className="form-group col-md-12">
                                    <label>Question Type</label>
                                    <select id="questionType" className="form-control" onClick={this.handleTypeChange} required>
                                        {this.state.questionTypes.map((type) => {
                                            return (
                                                <option key={type.uuid} value={type.type}>{type.type}</option>
                                            )
                                        })}

                                    </select>
                                </div>

                                {(this.state.selectedType === 'Open' || this.state.selectedType === 'Currency' || this.state.selectedType === 'Number') ?
                                    <div className="form-group col-md-12">
                                    </div>

                                    : (this.state.selectedType === 'MCQ') ?

                                        <div className="form-group col-md-12">
                                            <label>Options</label>
                                            <input type="text"
                                                className="form-control answer-inputs"
                                                placeholder='option 1'
                                                name='mcqAns1'
                                                value={this.state.mcqAns1}
                                                required
                                                onChange={this.handleChange}
                                            />
                                            <input type="text"
                                                className="form-control answer-inputs"
                                                placeholder='option 2'
                                                name='mcqAns2'
                                                value={this.state.mcqAns2}
                                                required
                                                onChange={this.handleChange}
                                            />
                                            <input type="text"
                                                className="form-control answer-inputs"
                                                placeholder='option 3'
                                                name='mcqAns3'
                                                value={this.state.mcqAns3}
                                                onChange={this.handleChange}
                                            />
                                            <input type="text"
                                                className="form-control answer-inputs"
                                                placeholder='option 4'
                                                name='mcqAns4'
                                                value={this.state.mcqAns4}
                                                onChange={this.handleChange}
                                            />
                                        </div>

                                        : (this.state.selectedType === 'Boolean') ?

                                            <div className="form-group col-md-12">
                                                <label>Yes / No</label>

                                                <input type="text"
                                                    className="form-control answer-inputs"
                                                    placeholder='option 1'
                                                    name='booleanAns1'
                                                    value={this.state.booleanAns1}
                                                    required
                                                    onChange={this.handleChange}
                                                />
                                                <input type="text"
                                                    className="form-control answer-inputs"
                                                    placeholder='option 2'
                                                    name='booleanAns2'
                                                    value={this.state.booleanAns2}
                                                    required
                                                    onChange={this.handleChange}
                                                />
                                            </div>

                                            : (this.state.selectedType === 'MCQ_Optional') ?

                                                <div className="form-group col-md-12">
                                                    <label>Options</label>

                                                    <input type="text"
                                                        className="form-control answer-inputs"
                                                        placeholder='option 1'
                                                        name='mcqOptionalAns1'
                                                        value={this.state.mcqOptionalAns1}
                                                        required
                                                        onChange={this.handleChange}
                                                    />
                                                    <input type="text"
                                                        className="form-control answer-inputs"
                                                        placeholder='option 2'
                                                        name='mcqOptionalAns2'
                                                        value={this.state.mcqOptionalAns2}
                                                        required
                                                        onChange={this.handleChange}
                                                    />
                                                    <input type="text"
                                                        className="form-control answer-inputs"
                                                        placeholder='option 3'
                                                        name='mcqOptionalAns3'
                                                        value={this.state.mcqOptionalAns3}
                                                        onChange={this.handleChange}
                                                    />
                                                    <input type="text"
                                                        className="form-control answer-inputs"
                                                        placeholder='option 4'
                                                        name='mcqOptionalAns4'
                                                        value={this.state.mcqOptionalAns4}
                                                        onChange={this.handleChange}
                                                    />

                                                </div>

                                                : null

                                }
                                <div className="form-group col-md-12">
                                    <button className='btn btn-success add-question-ans-btn'>Add</button>
                                </div>
                            </form>
                        </div>

                    </aside>
                </main >
            );
        } else {
            return null;
        }

    }
}


export default QuestionAnswers;