import React from 'react';
import { Link } from 'react-router-dom';
import Sidebar from './Sidebar';
import axios from 'axios';



class Users extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
        }
    }

    componentDidMount() {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/users`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    users: data,
                })
            })
            .catch((error) => console.error('Error ', error));
    }
    render() {

        return (
            <main className='d-flex flex-wrap justify-content-between'>
                <Sidebar />
                <section id='active-surveys'>
                    <table className='table table-dark table-hover'>
                        <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>User</th>
                                <th></th>
                                <th colSpan='2'>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.users.length > 0 && this.state.users.map((user, index) => {
                                return (
                                    <tr key={user.uuid}>
                                        <td>{index + 1}</td>
                                        <td><Link className='links' to={`/user-survey/${user.uuid}`}>{user.email}</Link></td>
                                        <td><Link className='links' to={`/user-survey/${user.uuid}`}>
                                            <button className='btn btn-sm btn-secondary'>View</button> </Link>
                                        </td>
                                        <td><button className='btn btn-sm btn-success'>Edit</button></td>
                                        <td><button className='btn btn-sm btn-danger'>Delete</button></td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </section>
            </main>
        )
    }
}
export default Users;