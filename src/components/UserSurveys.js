import React from 'react';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import axios from 'axios';




class UserSurveys extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userSurveys: [],
        }
        this.userUuid = props.match.params.id;
    }
    componentDidMount() {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/user-submitted-surveys/${this.userUuid}`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    userSurveys: data,
                })
            })
            .catch((error) => console.error('Error ', error));
    }
    render() {
        return (
            <section id='active-surveys'>

                <table className='table table-dark table-hover'>
                    <thead>
                        <tr>
                            <th>Sl No.</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Open date</th>
                            <th>Close date</th>
                            <th>
                                <Link to='/users'>
                                    <button className='btn btn-md btn-success survey-submit-btn'>Back</button>
                                </Link>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.userSurveys.length > 0 && this.state.userSurveys.map((survey, index) => {
                            return (
                                <tr key={survey.uuid}>
                                    <td>{index + 1}</td>
                                    <td><Link to={`/user-review-survey/${survey.uuid}/user/${this.userUuid}`} className='links'>{survey.title}</Link></td>
                                    <td>{survey.status}</td>
                                    <td><Moment format={'DD/MM/YYYY'}>{survey.open_date}</Moment></td>
                                    <td><Moment format={'DD/MM/YYYY'}>{survey.close_date}</Moment></td>

                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </section>
        )
    }
}
export default UserSurveys;