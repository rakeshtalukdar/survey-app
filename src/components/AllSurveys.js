import React from 'react';
import Moment from 'react-moment';
import Sidebar from './Sidebar';
import axios from 'axios';


class AllSurveys extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allSurveys: [],
            dataLoaded: false,
            msg: "",
            errors: false,
            statusChanged: false,
            isDeleted: false,
        }
    }

    componentDidMount() {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/surveys`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    allSurveys: data,
                    dataLoaded: true,
                })
            })
            .catch((error) => console.error('Error ', error));
    }

    handleView = (event, uuid) => {
        event.preventDefault();
        this.props.history.push(`/create-survey/${uuid}/question-answers`);
    };

    handleEdit = (event, uuid) => {
        event.preventDefault();

        this.props.history.push({
            pathname: `/all-surveys/${uuid}/edit`,
            state: { surveyUuid: uuid }
        });
    };

    handleDelete = async (event, uuid) => {
        event.preventDefault();
        try {
            const response = await axios.delete(`https://peaceful-retreat-69027.herokuapp.com/surveys/${uuid}`)
            if (response.status === 200) {
                const result = await response.data;
                alert(result.msg)
                this.setState({
                    msg: result.msg,
                    errors: false,
                    isDeleted: true,
                });

            } else {
                this.setState({
                    errors: true,
                });
            }

        } catch (error) {
            this.setState({
                errors: true,
            });
        }

    };

    handleStatusChange = async (event, uuid) => {
        event.preventDefault();
        const data = {
            uuid,
            status: event.target.value
        };
        try {
            const response = await axios(
                {
                    method: 'patch',
                    url: `https://peaceful-retreat-69027.herokuapp.com/surveys/change-status`,
                    data,
                });
            if (response.status === 200) {
                const result = await response.data;
                this.setState({
                    msg: result.msg,
                    errors: false,
                });

            }

        } catch (error) {
            this.setState({
                errors: true,
            });
        }
    }

    render() {

        return (
            <main className='d-flex flex-wrap justify-content-between'>
                <Sidebar />
                <section id='active-surveys'>
                    <table className='table table-dark table-hover'>
                        <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Open date</th>
                                <th>Close date</th>
                                <th colSpan='2'>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            {this.state.allSurveys.length > 0 && this.state.allSurveys.map((survey, index) => {
                                let btnClass = '';
                                if (survey.status === 'active') {
                                    btnClass = 'btn btn-outline-success btn-nd';
                                } else if (survey.status === 'closed') {
                                    btnClass = 'btn btn-outline-danger btn-md';
                                } else {
                                    btnClass = 'btn btn-outline-secondary btn-md';
                                }

                                return (
                                    <tr key={survey.uuid}>
                                        <td>{index + 1}</td>
                                        <td>{survey.title}</td>
                                        <td>{survey.category}</td>
                                        <td><button className={`${btnClass} text-white`}
                                            value={survey.status}
                                            onClick={(event) => this.handleStatusChange(event, survey.uuid)}>
                                            {survey.status}</button>
                                        </td>
                                        <td><Moment format={'DD/MM/YYYY'}>{survey.open_date}</Moment></td>
                                        <td><Moment format={'DD/MM/YYYY'}>{survey.close_date}</Moment></td>
                                        <td>
                                            <button onClick={(event) => this.handleView(event, survey.uuid)} className='btn btn-sm btn-secondary'>View</button>

                                        </td>
                                        <td><button onClick={(event) => { this.handleEdit(event, survey.uuid) }} className='btn btn-sm btn-success'>Edit</button></td>
                                        <td><button onClick={(event) => this.handleDelete(event, survey.uuid)} className='btn btn-sm btn-danger'>Delete</button></td>
                                    </tr>
                                );
                            })}

                        </tbody>
                    </table>
                </section>

            </main>
        )
    }
}
export default AllSurveys;