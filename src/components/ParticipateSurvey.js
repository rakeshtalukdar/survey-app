import React from 'react';
import Cookies from 'js-cookie'
import axios from 'axios';



class ParticipateSurvey extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            survey: [],
            allAnswers: [],
            allQuestions: [],
            errors: false,
            errorMsg: '',
            currencyError: false,
            response: {},
            isCompleted: true,
            counter: 0,
            closeDate: null,
            openDate: null,
            currentDate: null,
        }
        this.surveyUuid = props.match.params.id;
    }
    componentDidMount() {
        this.getSurvey();
        this.getAllQuestions();
        this.getAllAnswers();
    }

    getSurvey() {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/surveys/${this.surveyUuid}`)
            .then((response) => response.data)
            .then((data) => {
                let changeFormat1 = new Date(data[0].close_date);
                let changeFormat2 = new Date(data[0].open_date);
                let fullCloseDate = `${changeFormat1.getFullYear()}-${changeFormat1.getMonth() + 1}-${changeFormat1.getDate()}`;
                let fullOpenDate = `${changeFormat2.getFullYear()}-${changeFormat2.getMonth() + 1}-${changeFormat2.getDate()}`;
                let date = new Date();
                let currentFullDate = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;

                this.setState({
                    survey: data,
                    closeDate: fullCloseDate,
                    openDate: fullOpenDate,
                    currentDate: currentFullDate,

                });
            })
            .catch((error) => console.error('Error ', error));
    }
    getAllQuestions = () => {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/${this.surveyUuid}/all-questions`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    allQuestions: data,
                });
            })
            .catch((error) => console.error('Error ', error));
    }

    getAllAnswers = () => {
        axios.get(`https://peaceful-retreat-69027.herokuapp.com/all-answers`)
            .then((response) => response.data)
            .then((data) => {
                this.setState({
                    allAnswers: data,
                });
            })
            .catch((error) => console.error('Error ', error));
    }



    handleSubmitResponse = async (event) => {
        event.preventDefault();
        const data = {
            response: this.state.response,
        }

        if (Object.keys(data.response).length > 0) {
            const response = await axios({
                method: 'POST',
                url: `https://peaceful-retreat-69027.herokuapp.com/${this.surveyUuid}/save-response`,
                data
            })
            if (response.status === 200) {
                await response.data;
                Cookies.set('isCompleted', this.surveyUuid);
                this.setState({
                    errors: false,
                });
            }
        } else {
            alert("You have not given any input")
        }
    }


    handleAnswer = (event) => {

        event.persist();
        this.setState(prevState => {
            let response = prevState.response;
            response[event.target.name] = event.target.value;
            return response;
        });

    }


    render() {
        const cookie = Cookies.get('isCompleted');
        if (cookie !== this.surveyUuid) {
            if (this.state.closeDate !== null && this.state.openDate !== null) {
                if (new Date(this.state.currentDate) > new Date(this.state.closeDate) || new Date(this.state.currentDate) < new Date(this.state.openDate)) {
                    return (
                        <div id="survey-completed-msg">
                            <h1>This survey is either closed or not opened yet!!</h1>
                        </div>
                    )
                }
                else {
                    return (
                        <section id="question-answer-field">
                            <h1 className='heading'>Welcome To Our Survey</h1>
                            <form className='admin-question-answer' onSubmit={this.handleSubmitResponse}>
                                {this.state.allQuestions.length > 0 && this.state.allQuestions.map((question, index) => {
                                    return (
                                        <div key={index + 1} className="admin-survey-card card">
                                            <h5 className="card-header">{index + 1}. {question.question}?</h5>
                                            <div className="card-body">
                                                <ul key={question.uuid + 10} className='list-group'>
                                                    {this.state.allAnswers.map((answer, index) => {

                                                        if (question.uuid === answer.question_uuid) {
                                                            if (question.qType === 'Open') {
                                                                return (
                                                                    <li key={answer.uuid} className='list-group-item'>
                                                                        <textarea id={answer.uuid} required
                                                                            onChange={this.handleAnswer}
                                                                            name={question.uuid}
                                                                            className="card-text" />
                                                                    </li>
                                                                );
                                                            }

                                                            if (question.qType === 'Number') {
                                                                return (
                                                                    <li key={answer.uuid} className='list-group-item'>
                                                                        <input type="number"
                                                                            id={answer.uuid} required
                                                                            onChange={this.handleAnswer}
                                                                            name={question.uuid}
                                                                            className="card-text"
                                                                            placeholder='Enter a valid number'
                                                                            pattern="[0-9]*"
                                                                        />
                                                                    </li>
                                                                );
                                                            }

                                                            if (question.qType === 'Currency') {
                                                                return (
                                                                    <li key={answer.uuid} className='list-group-item'>

                                                                        <input type="text"
                                                                            id={answer.uuid} required
                                                                            onChange={this.handleAnswer}
                                                                            name={question.uuid}
                                                                            className="card-text currency"
                                                                            placeholder='Enter a valid currency'
                                                                            pattern="[0-9].*"
                                                                        />
                                                                    </li>
                                                                );
                                                            }

                                                            if (question.qType === 'MCQ_Optional') {
                                                                return (
                                                                    <li key={answer.uuid} className='list-group-item'>
                                                                        <label>
                                                                            <input key={index} id={answer.uuid}
                                                                                onChange={this.handleAnswer}
                                                                                type='radio' name={question.uuid}
                                                                                className="card-text"
                                                                                value={answer.answer} />
                                                                            {answer.answer}
                                                                        </label>
                                                                    </li>
                                                                );
                                                            } else {
                                                                return (
                                                                    <li key={answer.uuid + 1} className='list-group-item'>
                                                                        <label>
                                                                            <input key={index} id={answer.uuid}
                                                                                onChange={this.handleAnswer} required
                                                                                type='radio' name={question.uuid}
                                                                                className="card-text"
                                                                                value={answer.answer} />
                                                                            {answer.answer}
                                                                        </label>
                                                                    </li>
                                                                )
                                                            }
                                                        } else {
                                                            return null;
                                                        }
                                                    })}
                                                </ul>
                                            </div>
                                        </div>
                                    )
                                })}
                                <div className='form-group col-md-12 m-auto'>
                                    <label>Email</label>
                                    <input type='email' onChange={this.handleAnswer}
                                        className='form-control'
                                        required
                                        name='user' placeholder='Enter your email' />
                                </div>
                                <div>
                                    <button className='btn btn-md btn-success survey-submit-btn'>Submit</button>
                                </div>
                            </form>
                        </section>
                    )
                }

            } else {
                return null;
            }
        } else {
            return (
                <div id="survey-completed-msg">
                    <h1>You have completed the survey!!</h1>
                </div>
            )
        }

    }
}
export default ParticipateSurvey;