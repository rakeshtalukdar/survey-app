import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';


import Navbar from './components/Navbar';
import ActiveSurveys from './components/ActiveSurveys';
import ClosedSurveys from './components/ClosedSurveys';
import AllSurveys from './components/AllSurveys';
import EditSurvey from './components/EditSurvey';
import SubmittedSurveys from './components/SubmittedSurveys';
import Users from './components/Users';
import UserSurveys from './components/UserSurveys';
import UserReviewSurvey from './components/UserReviewSurvey';
import Surveys from './components/Surveys';
import Dashboard from './components/Dashboard';
import CreateSurvey from './components/CreateSurvey';
import QuestionAnswers from './components/QuestionAnswers';
import ParticipateSurvey from './components/ParticipateSurvey';
import Home from './components/Home';

class App extends React.Component {

  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/surveys' component={Surveys} />
            <Route exact path='/all-surveys' component={AllSurveys} />
            <Route path='/all-surveys/:id/edit' component={EditSurvey} />
            <Route exact path='/active-surveys' component={ActiveSurveys} />
            <Route exact path='/closed-surveys' component={ClosedSurveys} />
            <Route exact path='/submitted-surveys' component={SubmittedSurveys} />
            <Route exact path='/users' component={Users} />
            <Route exact path='/user-survey/:id' component={UserSurveys} />
            <Route exact path='/user-review-survey/:id/user/:userId' component={UserReviewSurvey} />
            <Route exact path='/dashboard' component={Dashboard} />
            <Route exact path='/create-survey' component={CreateSurvey} />
            <Route exact path='/create-survey/:id/question-answers' component={QuestionAnswers} />
            <Route exact path='/participate-survey/:id' component={ParticipateSurvey} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
